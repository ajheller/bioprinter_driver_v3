# README #

This is the Arduino Sketch to run the BioCurious BioPrinter.

For more information about the BioPrinter, see the [project wiki](https://sites.google.com/site/bioprinterwiki/)

The code has been tested on an
[Arduino Uno R3 SMD](http://arduino.cc/en/Main/arduinoBoardUno) with an
[Adafruit Motor Shield v2](http://www.adafruit.com/products/1438).

## Pin usage: ##

* D0  Serial comms (don't use this pin)
* D1  Serial comms (don't use this pin)
* D2  Pump 1 Reverse Limit Switch NC (Limit reached = HIGH)
* D3  Pump 1 Forward Limit Switch NC (Limit reached = HIGH)
* D4  Printhead Phase 1
* D5  Printhead Phase 2
* D6  Pump 1 Manual Reverse (active LOW)
* D7  Pump 1 Manual Forward (active LOW)
* D8  Pump 2 Manual Reverse (active LOW)
* D9  Pump 2 Manual Forward (active LOW)
* D10 Pump 2 Reverse Limit Switch NC (Limit reached = HIGH)
* D11 Pump 2 Forward Limit Switch NC (Limit reached = HIGH)
* D12 (unused!)
* D13 On board LED
* A0  (seems permanently tied low -- need to investigate further)
* A1  Test (run pumps at programmed rate)  used as digital input (active LOW)
* A2  Run  (run pumps under 3D printer control) used as digital input (active LOW)
* A3  (unused!)
* A4  (unused!)
* A5 SDA (serial comms to AF Motor Shield(s))
* A6 SCL (serial comms to AF Motor Shield(s))

All input pins except printhead (D4 & D5) should be tied to +VCC though 5k pull-up resistors to increase noise immunity.

## Hooking up stepper motors and power ##

Connect 12V from printer to power jack on Arduino. Remove VIN Jumper
from Motorshield. Connect 5V from printer to to Motor Power terminals
on Motor Shield (see diagram).

The current stepper motors have four wires, two for each
coil:

* Coil 1 Green and Blue
* Coil 2 Black and Red

With the board oriented so
the prototyping area is to the top, the motor connections are

           ------------------------------

    Green | M1                        M4 | Red
     Blue | M1                        M4 | Black
       NC | GND                      GND | NC
    Black | M2                        M3 | Blue
      Red | M2                        M3 | Green
                   + -
           --------| |-------------------
                   R B 
                   E L  5V from printer
                   D A
                     C
                     K


*Note:* This diagram refers to the color of the wires at the motors.
Some of the motors have their leads spliced onto other wires with
different colors.  If so, you will have to trace any spliced wires
back to the motor to get the correct color.

As of 30-Oct-2014 colors are

* M4 (outer) Brown
* M4 (inner) Red
* NC
* M3 (inner) Orange
* M3 (outer) Yellow

* M2 (outer) Black
* M2 (inner) Green
* NC
* M1 (inner) Orange
* M1 (outer) Blue

## Compiling the sketch and installing on Arduino Uno ##

* Install Arduino IDE and any drivers needed. [instructions](http://arduino.cc/en/Guide/HomePage)
* Install MotorShield
  library. [instructions](https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/install-software)
  *Note:* AccelStepper library is *not* used for the BioPrinter
  Syringe Pump.
* Retrive code from Git [repository](https://bitbucket.org/ajheller/bioprinter_driver_v3)
* Connect USB cable from your computer to the Arduino
* Load sketch into IDE and upload to Arduino [instructions](http://arduino.cc/en/Guide/Environment)

-------------------------------------------------------------------------------

Developers:

 * Aaron Heller <ajheller@gmail.com>
 * Isaac Heller <isaacjheller@gmail.com>