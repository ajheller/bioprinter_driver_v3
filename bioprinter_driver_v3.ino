/* 
 Bioprinter Syringe Driver v.3
 Isaac Heller <isaacjheller@gmail.com>
 Aaron Heller <ajheller@gmail.com> 
 7/16/2014
 
 For use with the Arduino Uno and Adafruit Motor Shield v2 
 ---->	http://www.adafruit.com/products/1438
 
 NOTE: To compile this code you must install the Adafruit 
 Motor Shield library.  See instructions at
   https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/install-software
 
 */

/* 
 === HOW TO SET THE SYRINGE PUMP RATES ===
 
 with the current syringe pumps
 the syringes dispense 1.5 mL/inch (measured)
 stepper motor moves 1 inch per 1000 steps (from spec sheet)
 
 so, if the desired rate is 0.1 mL/sec (= 100 uL/sec)
 then...
 
 (0.1 mL/sec) * (inch/1.5 mL) * (1000 steps/inch) * 1000 sec 
 = 0.1/1.5 * 1000 * 1000 = 66666.6666667
 
 This yeilds nominal rates for each stepper in steps per 1000 
 seconds.  Each iteration in loop(), this is multiplied by the
 elapsed time in microseconds to get the number of nanosteps to 
 add to the lag.  When the lag exceeds STEPPER_THRESH, we call
 onestep() and subtract STEPPER_THRESH from the lag.
 
 Nominal rates are then modulated by the period of the signal 
 comming from the 3D printer controller. Observed period of 
 that signal is 63 msec.
 
 */

const double programmed_stepper_rate[] = {
  66666.6666667, // rate for motor 0
  66666.6666667  // rate for motor 1
};

const double printhead_signal_period = 63000.0; // usec

/*******   No user servicable parts below here.   *********/
/*
 Copyright (c) 2014, Aaron J. Heller and Isaac J. Heller
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 3. Neither the name of the copyright holder nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <Wire.h>
#include <Adafruit_MotorShield.h>

#define VERBOSE 0

#define N_STEPPERS 2
#define N_AFMS int((N_STEPPERS+1)/2)  // each AFMS constrols two steppers


#define STEPPER_MODE DOUBLE // 1000 steps/inch

#if STEPPER_MODE == DOUBLE || STEPPER_MODE == SINGLE
#define STEPPER_THRESH 1000000000L
#elif STEPPER_MODE == INTERLEAVE 
#define STEPPER_THRESH 1000000000L/2
#elif STEPPER_MODE == MICROSTEP
#define STEPPER_THRESH 1000000000L/16
#endif

#define TEST 2
#define RUN  1
#define OFF  0

// macros for debugging
#define VERBOSE 0
#if VERBOSE
#define dprint(x) Serial.print(x)
#define dprintln(x) Serial.println(x)
#else
#define dprint(x)
#define dprintln(x)
#endif

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS[N_AFMS] = {
  Adafruit_MotorShield(0x60) }; 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Connect a stepper motor with 200 steps per revolution (1.8 degree)
// to motor ports #1 (M1 and M2) and #2 (M3 and M4)
Adafruit_StepperMotor* const myMotor[N_STEPPERS] = { 
  AFMS[0].getStepper(2000, 1), 
  AFMS[0].getStepper(2000, 2) };


unsigned long stepper_rate[N_STEPPERS];

void set_stepper_rate(const float factor = 1.0f) {
  for (byte i=0; i<N_STEPPERS; ++i) {
    stepper_rate[i] = long( programmed_stepper_rate[i]*factor + 0.5);
  }
}

// ----
unsigned long stepper_lag[N_STEPPERS];

void set_stepper_lag(const unsigned long val = 0) {
  for (byte i=0; i<N_STEPPERS; ++i) stepper_lag[i] = 0L;
}

// pump timer
unsigned long t0,t1,dt; 

// printhead input timer
unsigned long pt0=0, pt1=0;

// switch timer
unsigned long st0=0, st1=0;

// Pin assignements
// ---> Don't use A4 & A5 (Uno), D2 & D3 (leonardo), these are SDA and SCL
// ---> D9, D10 are used for Servos (which we don't use)
// ---> D13 has LED!
// ---> D0, D1 used for serial communication with host
// ---> A0 doesn't work either (ajh,7/17/14) FIXME: need to investigate further

// currently unused pins: D12, D13, A0, A3

// inputs for MODE switch
const byte pinTest = A1;
const byte pinRun  = A2;

// input pins for MANUAL operation 
const byte pinMRev[N_STEPPERS] = {
  6,8};
const byte pinMFwd[N_STEPPERS] = {
  7,9};

// input pins for LIMIT SWITCHES
const byte pinMLimitRew[N_STEPPERS] = {
  2,10};
const byte pinMLimitFwd[N_STEPPERS] = {
  3,11};

// input pins for extruder stepper phases
const byte pinXp1 = 4;
const byte pinXp2 = 5;

// globals to hold the current and previous states of the extruder phases
byte p1, p1_prev, p2, p2_prev;

// records if we've detected limit switches on each stepper
byte limitSwitchesPresent[N_STEPPERS];

byte digitalReadLimitSwitch(const byte m, const byte dir) {
  byte retval = 0;
  switch (dir) {
  case FORWARD:
    //switch is NC, opens with limit is reached
    retval = ! digitalRead(pinMLimitFwd[m]);
    break;
  case BACKWARD:
    retval = ! digitalRead(pinMLimitRew[m]);
    break;
  default:
    retval =  (! digitalRead(pinMLimitRew[m])) || (! digitalRead(pinMLimitFwd[m]));
    break;
  }
  return retval;
}
  
    

void reset_syringe_pump(const byte m, const byte delay_ms=3) {
  // reset the syringes
  //  limit switches are NC (LOW) and go HI when limit is reached

  // check for limit switches on this pump
  // since inputs are pulled high, and limit switches are wired NC
  // at least one switch will be LOW if there are limit switches 
  // on this pump
  limitSwitchesPresent[m] = !digitalRead(pinMLimitFwd[m]) || !digitalRead(pinMLimitRew[m]);
  Serial.println(limitSwitchesPresent[m]);

  // if there is a limit switch, run the pumps to both limits
  if (limitSwitchesPresent[m] ) {
    Serial.print("Limit switch on pump #");
    Serial.println(m);
    // run to FWD limit
    while(1) {
      if (digitalRead(pinMLimitFwd[m])) break;
      myMotor[m]->onestep(FORWARD, DOUBLE);
      delay(delay_ms);
    }
    // run to REW limit
    while(1) {
      if (digitalRead(pinMLimitRew[m])) break;
      myMotor[m]->onestep(BACKWARD, DOUBLE);
      delay(delay_ms);
    }
    // power down motor
    myMotor[m]->release();
  }
  else {
    Serial.print("No limit switch on pump #");
    Serial.println(m);
  }
} // end reset_sytinge_pump

void reset_syringe_pumps() {
  for(byte m=0; m<N_STEPPERS; ++m) {
    reset_syringe_pump(m);
  }
}

void print_banner() {
  delay(250);
  Serial.println(); 
  Serial.println("o------------------------------------------o");
  Serial.println("| BioCurious Bioprinter Syringe Driver V.3 |");
  Serial.println("| Isaac Heller and Aaron Heller, 8/2014    |");
  Serial.println("o------------------------------------------o");
  delay(250);
}

void setup() {
  // set up Serial library at 115200 bps (fastest speed in IDE)
  Serial.begin(115200); 

  print_banner();

  // magic incantation to change the i2c clock to 400KHz  
  TWBR = ((F_CPU /400000l) - 16) / 2; 

  // initialize each Motorshield
  for (byte i=0; i<N_AFMS; ++i) AFMS[i].begin(1600); // initailze AFMS w/PWM freq = 1.6kHz

  // setup MANUAL MODE pins
  pinMode(pinTest, INPUT_PULLUP);
  pinMode(pinRun,  INPUT_PULLUP);

  for (byte m=0; m<N_STEPPERS; ++m) {
    // setup manual pins
    pinMode(pinMRev[m], INPUT_PULLUP);
    pinMode(pinMFwd[m], INPUT_PULLUP);

    // setup switch pins
    pinMode(pinMLimitRew[m], INPUT_PULLUP);
    pinMode(pinMLimitFwd[m], INPUT_PULLUP);
  }

  // initialize printhead input pins
  pinMode(pinXp1, INPUT_PULLUP);
  pinMode(pinXp2, INPUT_PULLUP);

  reset_syringe_pumps();
  Serial.println("Syringe pumps reset!");

  p1 = p1_prev = digitalRead(pinXp1);
  p2 = p2_prev = digitalRead(pinXp2);

  // must be the last thing in setup
  set_stepper_rate(0);
  set_stepper_lag(0);
  t1 = micros();
}  // end setup

// this is the main calculation that decides when to step the motors
// stepper_lag keeps track of how many nanosteps each stepper is behind
// when we accumulate 10^9 nanosteps, we step the motor once and subtract 
// 10^9 from the lag.

void process_steppers(const unsigned long dt, const unsigned long t1) {
  for (byte i=0; i<N_STEPPERS; ++i) {
    stepper_lag[i] += stepper_rate[i] * dt;
    if ( stepper_lag[i] > STEPPER_THRESH ) {
      myMotor[i]->onestep(FORWARD, STEPPER_MODE);
      stepper_lag[i] -= STEPPER_THRESH;

      dprint("step #");
      dprintln(i);
    }
  }
}

void process_printhead_inputs(const unsigned long t1) {

  p1_prev = p1;
  p2_prev = p2;
  p1 = digitalRead(pinXp1);
  p2 = digitalRead(pinXp2);

  unsigned long dpt = t1 - pt0;

  if ( (p1 != p1_prev) || (p2 != p2_prev) ) {
    // we've seen the input change
    // calculate the new rate factor from the period

    double factor = printhead_signal_period/double(dpt);

    set_stepper_rate(factor);  //FIXME FIXME!

    {
      // debugging info
      dprint("----->");
      dprint(dpt);
      dprint(" ");
      dprint(factor);
      dprint(" ");
      dprint(p1); 
      dprint(p2);
      dprint(" ");
      dprint(p1 != p1_prev); 
      dprint(p2 != p2_prev);
      dprintln();
    }

    pt0 = t1;
  }
  else {
    if (dpt > 500000) {
      // we haven't heard from the stepper in 500 mS
      set_stepper_rate(0.0f);
      set_stepper_lag(0);

      {
        dprint("----->");
        dprint(dpt);
        dprint(" ");
        dprintln("Stop motors!!");
      }
      pt0 = t1;
    }
  }
}


byte process_mode_switch(const unsigned long t1) {

  const int mode_test = digitalRead(pinTest);
  const int mode_run  = digitalRead(pinRun);

  byte mode;

  if (! mode_test )      
    mode = TEST;
  else if (! mode_run )  mode = RUN;
  else                   mode = OFF;

  return(mode);
}


void loop() {
  // must be the first thing in loop
  t0 = t1;
  t1 = micros();
  dt = t1 - t0;

  //dprint("dt = "); 
  //dprintln(dt); 

  // every 100ms
  if ((t1 - st0) > 100000) {
    st0 = t1;
    byte mode = process_mode_switch(t1);

    // check the limit switches, if any are HIGH (limit reached) set mode to OFF
    for (byte m=0; m<N_STEPPERS; ++m) {
      if (limitSwitchesPresent[m] && ((mode==RUN) || (mode==TEST))  ) 
        if ( digitalRead(pinMLimitFwd[m])) {
          mode = OFF;
          Serial.print("Limit reached #");
          Serial.println(m);
          break;
        }
    }

    // decode the mode
    switch (mode) {
    case RUN: 
      dprintln("-------------> R U N   <----");
      process_printhead_inputs(t1);
      break;

    case TEST:
      dprintln("-------------> T E S T <----");
      set_stepper_rate(1.0f);
      break;

    case OFF:
      dprintln("-------------> O F F   <----");
      set_stepper_rate(0.0f);
      set_stepper_lag(0);

      for (byte i=0; i<N_STEPPERS; ++i) {
        myMotor[i]->release();
      }
      // process switches for manual mode
      while (true) {
        byte manual = false;
        for (byte m=0; m<N_STEPPERS; ++m) {
          if ( ! digitalRead(pinMRev[m])) {
            myMotor[m]->onestep(BACKWARD,DOUBLE);
            manual = true;
          }
          else if ( ! digitalRead(pinMFwd[m])) {
            myMotor[m]->onestep(FORWARD,DOUBLE);
            manual = true;
          }
        }
        if (manual)
          delay(5);
        else
          break;
      }
      break;
    } // end switch
  }
  process_steppers(dt,t1);
}

